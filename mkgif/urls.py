
from django.urls import path
from . import views

app_name = 'mkgif'

urlpatterns = [
    path('', views.index, name='index'),
    path('details/<int:pk>/', views.details, name='details'),
    path('details/<int:pk>/edit', views.edit_name, name='edit_name')
]
