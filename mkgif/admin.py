from django.contrib import admin
from mkgif.models import Animation, Image

# Register your models here.
admin.site.register(Animation)
admin.site.register(Image)
