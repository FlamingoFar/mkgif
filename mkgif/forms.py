from django.forms import ModelForm
from mkgif.models import Animation


class EditNameForm(ModelForm):
    class Meta:
        model = Animation
        fields = ('name',)
