from django.shortcuts import render, reverse
from .models import Animation, Image
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.conf import settings
from mkgif.forms import EditNameForm
import shutil

# Create your views here.

MEDIA_ROOT=settings.MEDIA_ROOT

@login_required
def index(request):
    if request.method == 'POST':
        anim = Animation.objects.create(name=request.POST['name'], user=request.user)
        for img in request.FILES.getlist('imgs'):
            Image.objects.create(animation=anim, image=img)
        anim.enqueue({})

    anims = Animation.objects.all()
    context = {
            'anims':anims
            }
    return render(request, 'mkgif/index.html', context)

@login_required
def details(request, pk):
    edit_name_form = None
    if request.method == 'DELETE':
        anim = get_object_or_404(Animation, pk=pk, user=request.user)
        shutil.rmtree(f"{MEDIA_ROOT}/{pk}")
        anim.delete()

        return HttpResponseRedirect(reverse('mkgif:index'))

    if request.method == 'POST':
        anim = Animation.objects.get(pk=pk)

        edit_name_form = EditNameForm(request.POST, instance=anim)
        if edit_name_form.is_valid():
            edit_name_form.save()
            edit_name_form = None
        return HttpResponseRedirect(reverse('mkgif:index'))

    anim = get_object_or_404(Animation, pk=pk, user=request.user)
    images = Image.objects.filter(animation=pk)
    context = {
            'anim': anim,
            'images' : images
            }

    print(context["images"][0])
    return render(request, 'mkgif/details.html' , context)

@login_required
def edit_name(request, pk):
    form = EditNameForm()
    context = {'form':form, "pk":pk}
    return render(request, "mkgif/partials/edit_name_form.html", context)
